<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TransactionModel extends Model
{
    use HasFactory;
    protected $table = "transactions";
    protected $fillable = ["inasurance_price", "shipping_price", "total_price", "status", "resi", "user_id"];
}
