<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductGaleries extends Model
{
    use HasFactory;
    protected $table = "product_galeries";
    protected $fillable = ["photo", "products_id"];

    public function product()
    {
        return $this->belongsTo(Product::class);
    }
}
