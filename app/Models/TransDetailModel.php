<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TransDetailModel extends Model
{
    use HasFactory;
    protected $table = "transactions_details";
    protected $fillable = ["price", "quantity", "transaction_id", "product_id"];

}
