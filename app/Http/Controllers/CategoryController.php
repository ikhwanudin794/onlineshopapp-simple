<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;

use RealRashid\SweetAlert\Facades\Alert;


class CategoryController extends Controller
{
    public function index()
    {
        $categories = DB::table('categories')->get();

        return view('category.index', ['categories' => $categories]);
    }
    public function create(){
        return view('category.create');
    }
    public function store(Request $request){
        $request->validate([
            'name' => 'required',
        ]);

        DB::table('categories')->insert([
            'name' => $request['name'],
        ]);

        Alert::success('Berhasil', 'Sukses Menyimpan Category');
        return redirect('/categories');
    }

    public function edit($id){
        $categories = DB::table('categories')->where('id', $id)->first();

        return view('category.edit', ['categories' => $categories]);
    }

    public function update(Request $request, $id){
        $request->validate([
            'name' => 'required',
        ]);
        DB::table('categories')
                ->where('id', $id)
                ->update(
                    [
                        'name' => $request['name'],
                    ]);

        Alert::success('Berhasil', 'Sukses Mengedit Category');
        return redirect('categories');
    }

    public function destroy($id){
        DB::table('categories')->where('id', $id)->delete();

        Alert::success('Berhasil', 'Sukses Menghapus Category');
        return redirect('categories');
    }
}
