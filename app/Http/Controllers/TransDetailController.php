<?php

namespace App\Http\Controllers;

use App\Models\TransactionModel;
use App\Models\Product;
use App\Models\TransDetailModel;

use Illuminate\Http\Request;

class TransDetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $trans_details = TransDetailModel::all();
        return view('transaction-details.index', compact('trans_details'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $prod = Product::all();
        $tran = TransactionModel::all();

        return view(
            'transaction-details.create',
            ['products' => $prod],
            ['transactions' => $tran]
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'price' => 'required',
            'quantity' => 'required',
            'transaction_id' => 'required',
            'product_id' => 'required',
        ]);

        $trans_details = new TransDetailModel();

        $trans_details->price = $request->input('price');
        $trans_details->quantity = $request->input('quantity');

        $trans_details->transaction_id = $request->input('transaction_id');
        $trans_details->product_id = $request->input('product_id');

        $trans_details->save();

        return redirect('/transaction-details');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function show($id)
    {
        $trans = TransDetailModel::find($id);
        return view('transaction-details.show', ['transactions_details' => $trans]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $trans_details = TransDetailModel::find($id);
        $trans = TransactionModel::all();
        $prod = Product::all();
        return view(
            'transaction-details.edit',
            [
                'transactions_details' => $trans_details,
                'transactions' => $trans,
                'products' => $prod
            ]
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request, $id)
    {
        $request->validate([
            'price' => 'required',
            'quantity' => 'required',
            'transaction_id' => 'required',
            'product_id' => 'required',
        ]);

        $tran = TransDetailModel::find($id);

        $tran->price = $request->input('price');
        $tran->quantity = $request->input('quantity');
        $tran->transaction_id = $request->input('transaction_id');
        $tran->product_id = $request->input('product_id');

        $tran->save();

        return redirect('/transaction-details');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $a = TransDetailModel::find($id);
        $a->delete();
        return redirect('/transaction-details');
    }
}
