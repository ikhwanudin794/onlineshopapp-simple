<?php

namespace App\Http\Controllers;
use App\Models\Product;
use App\Models\ProductGaleries;
use Illuminate\Http\Request;
use File;

use RealRashid\SweetAlert\Facades\Alert;

class ProductGaleriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $productGaleries = ProductGaleries::all();
       return view('product-galeries.index', ['productGaleries' => $productGaleries]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $products = Product::all();

        return view('product-galeries.create', ['products' => $products]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'photo' => 'required|mimes:jpg,png,jpeg|max:2048',
            'products_id' => 'required',
        ]);

        $photoName = time().'.'.$request->photo->extension();
        $request->photo->move(public_path('images'), $photoName);

        $productGaleries = new ProductGaleries;

        $productGaleries->photo = $photoName;
        $productGaleries->products_id = $request->input('products_id');

        $productGaleries->save();
        Alert::success('Berhasil', 'Sukses Menyimpan Galery');
        return redirect('/product-galeries');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $productGaleries = ProductGaleries::find($id);
        $products = Product::all();
        return view('product-galeries.edit', [ 'productGaleries'=>$productGaleries, 'products' => $products]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'products_id' => 'required',
        ]);

        $productGaleries = ProductGaleries::find($id);
        if ($request->has('photo')){
            $path = "images/";
            File::delete($path . $productGaleries->photo);

            $photoName = time().'.'.$request->photo->extension();
            $request->photo->move(public_path('images'), $photoName);

            $productGaleries->photo = $photoName;
        }
        $productGaleries->products_id = $request->input('products_id');
        $productGaleries->save();
        Alert::success('Berhasil', 'Sukses Mengedit Galery');
        return redirect('/product-galeries');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $productGaleries = ProductGaleries::find($id);
        $path = "images/";
        File::delete($path . $productGaleries->photo);
        $productGaleries->delete();
        Alert::success('Berhasil', 'Sukses Menghapus Galery');
        return redirect('/product-galeries');
    }
}
