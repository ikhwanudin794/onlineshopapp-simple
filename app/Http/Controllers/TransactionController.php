<?php

namespace App\Http\Controllers;

use App\Models\TransactionModel;
use App\Models\User;

use Illuminate\Http\Request;

class TransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $trans = TransactionModel::all();
        return view('transactions.index', compact('trans'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $trans = TransactionModel::all();
        $user = User::all();
        return view(
            'transactions.create',
            ['transactions' => $trans],
            ['users' => $user]
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'inasurance_price' => 'required',
            'shipping_price' => 'required',
            'total_price' => 'required',
            'status' => 'required',
            'resi' => 'required',
        ]);

        $trans = new TransactionModel();

        $trans->inasurance_price = $request->input('inasurance_price');
        $trans->shipping_price = $request->input('shipping_price');
        $trans->total_price = $request->input('total_price');
        $trans->status = $request->input('status');
        $trans->resi = $request->input('resi');

        $trans->user_id = $request->input('user_id');

        $trans->save();

        return redirect('/transaction');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function show($id)
    {
        $trans = TransactionModel::find($id);
        return view('transactions.show', ['transactions' => $trans]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $trans = TransactionModel::find($id);
        $user = User::all();
        return view('transactions.edit', ['transactions' => $trans, 'users' => $user]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request, $id)
    {
        $request->validate([
            'inasurance_price' => 'required',
            'shipping_price' => 'required',
            'total_price' => 'required',
            'status' => 'required',
            'resi' => 'required',
        ]);

        $user = TransactionModel::find($id);

        $user->inasurance_price = $request->input('inasurance_price');
        $user->shipping_price = $request->input('shipping_price');
        $user->total_price = $request->input('total_price');
        $user->status = $request->input('status');
        $user->resi = $request->input('resi');

        $user->user_id = $request->input('user_id');

        $user->save();

        return redirect('/transaction');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $trans = TransactionModel::find($id);
        $trans->delete();
        return redirect('/transaction');
    }
}
