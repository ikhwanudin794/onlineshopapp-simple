<div class="container-fluid mb-5">
    <div class="row border-top px-xl-5">
        <div class="col-lg-3 d-none d-lg-block">
            <a class="btn shadow-none d-flex align-items-center justify-content-between bg-primary text-white w-100" data-toggle="collapse" href="#navbar-vertical" style="height: 65px; margin-top: -1px; padding: 0 30px;">
                <h6 class="m-0">Admin</h6>
            </a>
            <nav class="collapse show navbar navbar-vertical navbar-light align-items-start p-0 border border-top-0 border-bottom-0" id="navbar-vertical">
                <div class="navbar-nav w-100 overflow-hidden" style="height: 410px">
                    <a href="/product" class="nav-item nav-link">Product</a>
                    <a href="/product-galeries" class="nav-item nav-link">Product Galeries</a>
                    <a href="/categories" class="nav-item nav-link">Categories</a>
                    <a href="/transaction" class="nav-item nav-link">Transaction</a>
                    <a href="/transaction-details" class="nav-item nav-link">Transaction Detail</a>
                </div>
            </nav>
        </div>
        <div class="col-lg-9">
            @yield('content')
        </div>
    </div>
</div>
