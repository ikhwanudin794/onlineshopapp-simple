@extends('masters.admin')


@section('judul')
Halaman edit cast
@endsection
@section('content')
<form action = '/categories/{{$categories->id}}' method = 'POST'>
    @csrf
    @method('PUT')
    <div class="form-group">
        <label>Name</label>
        <input type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{$categories->name}}">
    </div>
    @error('name')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection
