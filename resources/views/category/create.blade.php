@extends('masters.admin')
@section('title')
Halaman Tambah Kategori
@endsection

@section('content')
<form action="/categories" method="post">
    @csrf
    <div class="form-group">
        <label>Name</label>
        <input type="text" class="form-control @error('name') is-invalid @enderror" name="name">
    </div>
    @error('name')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <!-- <div class="form-group">
        <label>Description</label>
        <textarea class="form-control @error('description') is-invalid @enderror" name="description"></textarea>
    </div>
    @error('description')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror -->

  <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection
