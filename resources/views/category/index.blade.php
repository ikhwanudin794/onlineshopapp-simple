@extends('masters.admin')
@section('title')
Halaman List Categories
@endsection

@section('content')
<a href="/categories/create" class="btn btn-primary btn-sm my-3">+ Tambah</a>
<table class="table">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Name</th>

      <!-- <th scope="col">Price</th>
      <th scope="col">Description</th>
      <th scope="col">Categories_id</th>
      <th scope="col">Action</th> -->
    </tr>
  </thead>
  <tbody>
    @forelse ($categories as $key=>$value)
            <tr>
                <td>{{ $key + 1}}</td>
                <td>{{ $value->name }}</td>
                <td><a class ="btn btn-warning btn-sm" href="/categories/{{$value->id}}/edit">edit</a></td>
                <td>
                    <form action= "/categories/{{$value->id}}" method="POST">
                        @csrf
                        @method('DELETE')
                        <input type="submit" value="Delete" class="btn btn-danger btn-sm">
                    </form>
                </td>
            </tr>

        @empty
            <tr>
                <td>No data</td>
            </tr>

        @endforelse
  </tbody>
</table>
@endsection

