@extends('masters.admin')
@section('title')
Halaman List Produk Galeries
@endsection

@section('content')
<a href="/product-galeries/create" class="btn btn-primary btn-sm my-3">+ Tambah</a>
<table class="table">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Photo</th>
      <th scope="col">Produk</th>
      <th scope="col">Action</th>
    </tr>
  </thead>
  <tbody>
      @forelse ($productGaleries as $key=>$value)
        <tr>
            <td>{{$key + 1}}</th>
            <td>
                <img src="{{asset('images/' . $value->photo)}}" height="50px" alt="...">
            </td>
            <td>{{$value->products_id}}</td>
            <td>
                
                <form action="/product-galeries/{{$value->id}}" method="POST">
                    @csrf
                    @method('DELETE')
                    <a href="/product-galeries/{{$value->id}}/edit" class="btn btn-primary">Edit</a>
                    <input type="submit" class="btn btn-danger my-1" value="Delete">
                </form>
            </td>
        </tr>
    @empty
        <tr colspan="3">
            <td>No data</td>
        </tr>  
    @endforelse 
  </tbody>
</table>
@endsection

