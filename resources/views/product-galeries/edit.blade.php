@extends('masters.admin')
@section('title')
Halaman Edit Produk Galeries
@endsection

@section('content')
<form action="/product-galeries/{{$productGaleries->id}}" method="post" enctype="multipart/form-data">
    @csrf
    @method('put')
    <div class="form-group">
        <label>Photo</label>
        <input type="file" class="form-control @error('photo') is-invalid @enderror" name="photo">
    </div>
    @error('photo')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Product</label>
        <select name="products_id" class="form-control" id="">
            <option value="">--Pilih Produk--</option>
            @forelse ($products as $item)
                @if ($item->id === $productGaleries->products_id)
                <option value="{{$item->id}}" selected> {{$item->name}} </option>
                @else
                <option value="{{$item->id}}"> {{$item->name}} </option>
                @endif
            @empty
                <option value=""> Tidak Ada Produk </option>
            @endforelse
        </select>
    </div>
    @error('products_id')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection