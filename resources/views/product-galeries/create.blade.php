@extends('masters.admin')
@section('title')
Halaman Tambah Produk Galeries
@endsection

@section('content')
<form action="/product-galeries" method="post" enctype="multipart/form-data">
    @csrf
    <div class="form-group">
        <label>Photo</label>
        <input type="file" class="form-control @error('photo') is-invalid @enderror" name="photo">
    </div>
    @error('photo')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Product</label>
        <select name="products_id" class="form-control" id="">
            <option value="">--Pilih Produk--</option>
            @forelse ($products as $item)
                <option value="{{$item->id}}"> {{$item->name}} </option>
            @empty
                <option value=""> Tidak Ada Produk </option>
            @endforelse
        </select>
    </div>
    @error('products_id')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection