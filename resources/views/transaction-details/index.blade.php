@extends('masters.admin')
@section('title')
Page List Transactions Detail
@endsection

@section('content')
<a href="/transaction-details/create" class="btn btn-primary btn-sm my-3">+ Tambah</a>
<table class="table">
  <thead>
    <tr>
      <th scope="col">No.</th>
      <th scope="col">Price</th>
      <th scope="col">Quantity</th>
      <th scope="col">Trans ID</th>
      <th scope="col">Prod ID</th>
      <th scope="col">Action</th>
    </tr>
  </thead>
  <tfoot>
    <th scope="col">No.</th>
    <th scope="col">Price</th>
    <th scope="col">Quantity</th>
    <th scope="col">Trans ID</th>
    <th scope="col">Prod ID</th>
    <th scope="col">Action</th>
  </tfoot>
  <tbody>
      @forelse ($trans_details as $key => $value)
        <tr>
            <td>{{$key + 1}}</th>
            <td>{{$value->price}}</td>
            <td>{{$value->quantity}}</td>
            <td>{{$value->transaction_id}}</td>
            <td>{{$value->product_id}}</td>
            <td>
                
                <form action="/transaction-details/{{$value->id}}" method="POST">
                    @csrf
                    @method('DELETE')
                    <a href="/transaction-details/{{$value->id}}" class="btn btn-info">Detail</a>
                    <a href="/transaction-details/{{$value->id}}/edit" class="btn btn-primary">Edit</a>
                    <input type="submit" class="btn btn-danger my-1" value="Delete">
                </form>
            </td>
        </tr>
    @empty
        <tr colspan="3">
            <td>Empty</td>
        </tr>  
    @endforelse 
  </tbody>
</table>
@endsection
