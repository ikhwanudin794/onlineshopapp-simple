@extends('masters.admin')
@section('title')
Page List Transactions
@endsection

@section('content')

<form action="/transaction-details" method="get">
    @csrf
    <div class="form-group">
        <label>Price</label>
        <input readonly value="{{ $transactions_details->price }}" class="form-control @error('price') is-invalid @enderror" name="price">
    </div>
    <div class="form-group">
        <label>Quantity</label>
        <input readonly value="{{ $transactions_details->quantity }}" class="form-control @error('quantity') is-invalid @enderror" name="quantity">
    </div>
    <div class="form-group">
        <label>transaction_id</label>
        <input readonly value="{{ $transactions_details->transaction_id }}" class="form-control @error('transaction_id') is-invalid @enderror" name="transaction_id">
    </div>
    <div class="form-group">
        <label>product_id</label>
        <input readonly value="{{ $transactions_details->product_id }}"  class="form-control @error('product_id') is-invalid @enderror" name="product_id">
    </div>

  <button type="sand" class="btn btn-primary">Back</button>
</form>
    
@endsection