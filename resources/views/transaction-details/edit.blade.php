@extends('masters.admin')
@section('title')
Page Edit Transaction
@endsection

@section('content')
<form action="/transaction-details/{{ $transactions_details->id }}" method="POST">
    @csrf
    <div class="form-group">
        <label>Price</label>
        <input type="text" class="form-control @error('price') is-invalid @enderror" name="price">
    </div>
    @error('price')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Quantity</label>
        <input type="number" class="form-control @error('quantity') is-invalid @enderror" name="quantity">
    </div>
    @error('quantity')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Asurance</label>
        <select name="transaction_id" class="form-control" id="transaction_id">
            <option value="">Select</option>
            @forelse ($transactions as $item)
                <option value="{{$item->id}}"> {{$item->inasurance_price}} </option>
            @empty
                <option value=""> Empty </option>
            @endforelse
        </select> 
       </div>
    @error('transaction_id')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
        <label>Products</label>
        <select name="product_id" class="form-control" id="product_id">
            <option value="">Select</option>
            @forelse ($products as $item)
                <option value="{{$item->id}}"> {{$item->name}} </option>
            @empty
                <option value=""> Empty </option>
            @endforelse
        </select> 
       </div>
    @error('product_id')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror

  <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection