@extends('masters.master')

@section('judul')
Halaman Edit Profile
@endsection

@section('content')
<form action = '/profile' method = 'POST'>
    @csrf
    @method('PUT')
    <div class="form-group">
        <label>Name</label>
        <input type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{$user->name}}">
    </div>
    @error('name')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>telephone number</label>
        <textarea class="form-control @error('no_phone') is-invalid @enderror" name="no_phone">{{$user->no_phone}}</textarea>
    </div>
    @error('no_phone')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Address</label>
        <textarea class="form-control @error('address') is-invalid @enderror" name="address">{{$user->address}}</textarea>
    </div>
    @error('address')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection
