@extends('masters.master')

@section('title')
Home
@endsection
@section('content')
    <!-- Navbar Start -->
    @include('layouts.navbar')
    <!-- Navbar End -->


    <!-- Featured Start -->
    @include('partials.featured')
    <!-- Featured End -->


    <!-- Categories Start -->
    <!-- @include('partials.categories') -->
    <!-- Categories End -->


    <!-- Offer Start -->
    <!-- @include('partials.offer') -->
    <!-- Offer End -->


    <!-- Products Start -->
    @include('partials.products_top')
    <!-- Products End -->


    <!-- Subscribe Start -->
    <!-- @include('partials.subscribe') -->
    <!-- Subscribe End -->


    <!-- Products Start -->
    <!-- @include('partials.products_bottom') -->
    <!-- Products End -->


    <!-- Vendor Start -->
    <!-- @include('partials.vendor') -->
    <!-- Vendor End -->


    <!-- Footer Start -->
    @include('layouts.footer')
    <!-- Footer End -->
@endsection
