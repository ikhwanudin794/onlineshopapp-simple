@extends('masters.admin')
@section('title')
Page List Transactions
@endsection

@section('content')
<a href="/transaction/create" class="btn btn-primary btn-sm my-3">+ Tambah</a>
<table class="table">
  <thead>
    <tr>
      <th scope="col">No.</th>
      <th scope="col">Assurance</th>
      <th scope="col">Shipping</th>
      <th scope="col">Price</th>
      <th scope="col">Status</th>
      <th scope="col">Resi</th>
      <th scope="col">User</th>
      <th scope="col">Action</th>
    </tr>
  </thead>
  <tfoot>
    <th scope="col">No.</th>
    <th scope="col">Assurance</th>
    <th scope="col">Shipping</th>
    <th scope="col">Price</th>
    <th scope="col">Status</th>
    <th scope="col">Resi</th>
    <th scope="col">User</th>
    <th scope="col">Action</th>
  </tfoot>
  <tbody>
      @forelse ($trans as $key => $value)
        <tr>
            <td>{{$key + 1}}</th>
            <td>{{$value->inasurance_price}}</td>
            <td>{{$value->shipping_price}}</td>
            <td>{{$value->total_price}}</td>
            <td>{{$value->status}}</td>
            <td>{{$value->resi}}</td>
            <td>{{$value->user_id}}</td>
            <td>
                
                <form action="/transaction/{{$value->id}}" method="POST">
                    @csrf
                    @method('DELETE')
                    <a href="/transaction/{{$value->id}}" class="btn btn-info">Detail</a>
                    <a href="/transaction/{{$value->id}}/edit" class="btn btn-primary">Edit</a>
                    <input type="submit" class="btn btn-danger my-1" value="Delete">
                </form>
            </td>
        </tr>
    @empty
        <tr colspan="3">
            <td>Empty</td>
        </tr>  
    @endforelse 
  </tbody>
</table>
@endsection
