@extends('masters.admin')
@section('title')
Page List Transactions
@endsection

@section('content')

<form action="/transaction" method="get">
    @csrf
    <div class="form-group">
        <label>Asurance</label>
        <input readonly value="{{ $transactions->inasurance_price }}" class="form-control @error('inasurance_price') is-invalid @enderror" name="inasurance_price">
    </div>
    <div class="form-group">
        <label>Shipping</label>
        <input readonly value="{{ $transactions->shipping_price }}" class="form-control @error('shipping_price') is-invalid @enderror" name="shipping_price">
    </div>
    <div class="form-group">
        <label>Total Price</label>
        <input readonly value="{{ $transactions->total_price }}" class="form-control @error('total_price') is-invalid @enderror" name="total_price">
    </div>
    <div class="form-group">
        <label>Status</label>
        <input readonly value="{{ $transactions->status }}"  class="form-control @error('status') is-invalid @enderror" name="status">
    </div>
    <div class="form-group">
        <label>Resi</label>
        <input readonly value="{{ $transactions->resi }}" class="form-control @error('resi') is-invalid @enderror" name="resi">
    </div>
    <div class="form-group">
        <label>User</label>
        <input readonly value="{{ $transactions->user_id }}" class="form-control @error('user_id') is-invalid @enderror" name="user_id">
    </div>
  <button type="sand" class="btn btn-primary">Back</button>
</form>
    
@endsection