@extends('masters.admin')
@section('title')
Page List Transaction
@endsection

@section('content')
<form action="/transaction" method="POST">
    @csrf
    <div class="form-group">
        <label>Asurance</label>
        <input type="text" class="form-control @error('inasurance_price') is-invalid @enderror" name="inasurance_price">
    </div>
    @error('inasurance_price')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Shipping</label>
        <input type="number" class="form-control @error('shipping_price') is-invalid @enderror" name="shipping_price">
    </div>
    @error('shipping_price')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Total Price</label>
        <input type="number" class="form-control @error('total_price') is-invalid @enderror" name="total_price">
    </div>
    @error('total_price')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Status</label>
        <textarea class="form-control @error('status') is-invalid @enderror" name="status"></textarea>
    </div>
    @error('status')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
        <label>Resi</label>
        <input type="number" class="form-control @error('resi') is-invalid @enderror" name="resi">
    </div>
    @error('resi')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror


    <div class="form-group">
        <label>Users</label>
        <select name="user_id" class="form-control" id="user_id">
            <option value="">-- Users --</option>
            @forelse ($users as $item)
                <option value="{{$item->id}}"> {{$item->name}} </option>
            @empty
                <option value=""> Empty </option>
            @endforelse
        </select> 
       </div>
    @error('user_id')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror

  <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection