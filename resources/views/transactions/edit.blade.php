@extends('masters.admin')
@section('title')
Page Edit Transaction
@endsection

@section('content')
<form action="/transaction/{{$transactions->id}}" method="POST">

    @csrf
    @method('PUT')
    <div class="form-group">
        <label>Asurance Price</label>
        <input type="number" value="{{$transactions->inasurance_price}}"  class="form-control @error('inasurance_price') is-invalid @enderror" name="inasurance_price">
    </div>
    @error('inasurance_price')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Shipping Price</label>
        <input type="number" value="{{$transactions->shipping_price}}"  class="form-control @error('shipping_price') is-invalid @enderror" name="shipping_price">
    </div>
    @error('shipping_price')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Total Price</label>
        <input type="number" value="{{$transactions->total_price}}"  class="form-control @error('total_price') is-invalid @enderror" name="total_price">
    </div>
    @error('total_price')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Status</label>
        <textarea class="form-control @error('status') is-invalid @enderror" name="status">{{$transactions->status}} </textarea>
    </div>
    @error('status')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
        <label>Resi</label>
        <input type="number" value="{{$transactions->resi}}" class="form-control @error('rs') is-invalid @enderror" name="resi">
    </div>
    @error('resi')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
        <label>Users</label>
        <select name="user_id" class="form-control" id="user_id">
            <option value="">--Pilih Kategori--</option>
            @forelse ($users as $item)
                @if ($item->id === $transactions->user_id)
                <option value="{{$item->id}}" selected> {{$item->name}} </option>
                @else
                <option value="{{$item->id}}"> {{$item->name}} </option>
                @endif
            @empty
                <option value="user_id"> Tidak Ada Kategori </option>
            @endforelse
        </select>  
    </div>
    @error('user_id')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror

  <button type="submit" class="btn btn-primary">Submit</button>
  
</form>
@endsection