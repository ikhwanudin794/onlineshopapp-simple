<div class="container-fluid pt-5">
    <div class="text-center mb-4">
        <h2 class="section-title px-5"><span class="px-2">Products</span></h2>
    </div>
    <div class="row px-xl-5 pb-3">
        @forelse ($product as $item)
        <div class="col-lg-3 col-md-6 col-sm-12 pb-1">
            <div class="card product-item border-0 mb-4">
                @php
                $ada_galery = false;
                @endphp
                @foreach ($productGaleries as $value)
                    @if ($value->products_id == $item->id)
                        <div class="card-header product-img position-relative overflow-hidden bg-transparent border p-0">
                            <img class="img-fluid w-100" src="{{asset('images/' . $value->photo)}}" alt="">
                        </div>
                        @php
                        $ada_galery = true;
                        @endphp
                        @break
                    @endif
                @endforeach
                @if ($ada_galery == false)
                <h1>Tidak Ada Data Gambar</h1>
                @endif
                <div class="card-body border-left border-right text-center p-0 pt-4 pb-3">
                    <h6 class="text-truncate mb-3">{{$item->name}}</h6>
                    <div class="d-flex justify-content-center">
                        <h6>{{$item->price}}</h6>
                    </div>
                </div>
                <div class="card-footer d-flex justify-content-between bg-light border">
                    <a href="/product/{{$item->id}}" class="btn btn-sm text-dark p-0"><i class="fas fa-eye text-primary mr-1"></i>View Detail</a>
                    <a href="" class="btn btn-sm text-dark p-0"><i class="fas fa-shopping-cart text-primary mr-1"></i>Add To Cart</a>
                </div>
            </div>
        </div>
        @empty
            <h1>Tidak Ada Data Produk</h1>
        @endforelse
    </div>
</div>
