@extends('masters.master')
@section('title')
Halaman Detail Produk
@endsection

@section('content')
<div class="container-fluid">
<div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
  <div class="carousel-inner">
  @foreach ($productGaleries as $key => $value)
    <div class="carousel-item {{$key == 0 ? 'active' : '' }}">
      <img src="{{asset('images/' . $value->photo)}}" height="200px" class="d-block w-100" alt="...">
    </div>
    @endforeach
  </div>
 <button class="carousel-control-prev" type="button" data-target="#carouselExampleControls" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </button>
  <button class="carousel-control-next" type="button" data-target="#carouselExampleControls" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </button>
</div>
<h1 class="text-primary">{{$product->name}}</h1>
<h6>Kategori</h6>
@foreach ($categories as $item)
<span class="badge badge-pill badge-primary">{{$item->name}}</span>
@endforeach
<h5>Price: {{$product->price}}</h5>
<h6>Stock: {{$product->stock}}</h6>
<a href="" class="btn border">
    <i class="fas fa-shopping-cart text-primary"></i>
    <span class="badge"></span>
</a>
<p>{{$product->description}}</p>
</div>
@endsection