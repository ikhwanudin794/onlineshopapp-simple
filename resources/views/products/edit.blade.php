@extends('masters.admin')
@section('title')
Halaman Edit Produk
@endsection

@section('content')
<form action="/product/{{$product->id}}" method="POST">
    @csrf
    @method('put')
    <div class="form-group">
        <label>Name</label>
        <input type="text" value="{{$product->name}}" class="form-control @error('name') is-invalid @enderror" name="name">
    </div>
    @error('name')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Stock</label>
        <input type="number" value="{{$product->stock}}" class="form-control @error('stock') is-invalid @enderror" name="stock">
    </div>
    @error('stock')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Price</label>
        <input type="number" value="{{$product->price}}" class="form-control @error('price') is-invalid @enderror" name="price">
    </div>
    @error('price')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Description</label>
        <textarea class="form-control @error('description') is-invalid @enderror" name="description">{{$product->description}}</textarea>
    </div>
    @error('description')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Kategori</label>
        <select name="categories_id" class="form-control" id="">
            <option value="">--Pilih Kategori--</option>
            @forelse ($categories as $item)
                @if ($item->id === $product->categories_id)
                <option value="{{$item->id}}" selected> {{$item->name}} </option>
                @else
                <option value="{{$item->id}}"> {{$item->name}} </option>
                @endif
            @empty
                <option value=""> Tidak Ada Kategori </option>
            @endforelse
        </select>
    </div>
    @error('categories_id')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror

  <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection

@push('script')
<script src="https://cdn.tiny.cloud/1/myc48f8gtxam3bxztbq90veuagr4x54kb3qwfo4cgp461qek/tinymce/6/tinymce.min.js" referrerpolicy="origin"></script>
<script>
    tinymce.init({
      selector: 'textarea',
      plugins: 'ai tinycomments mentions anchor autolink charmap codesample emoticons image link lists media searchreplace table visualblocks wordcount checklist mediaembed casechange export formatpainter pageembed permanentpen footnotes advtemplate advtable advcode editimage tableofcontents mergetags powerpaste tinymcespellchecker autocorrect a11ychecker typography inlinecss',
      toolbar: 'undo redo | blocks fontfamily fontsize | bold italic underline strikethrough | link image media table mergetags | align lineheight | tinycomments | checklist numlist bullist indent outdent | emoticons charmap | removeformat',
      tinycomments_mode: 'embedded',
      tinycomments_author: 'Author name',
      mergetags_list: [
        { value: 'First.Name', title: 'First Name' },
        { value: 'Email', title: 'Email' },
      ],
      ai_request: (request, respondWith) => respondWith.string(() => Promise.reject("See docs to implement AI Assistant"))
    });
  </script>

@endpush
