@extends('masters.admin')
@section('title')
Halaman List Produk
@endsection

@section('content')
<a href="/product/create" class="btn btn-primary btn-sm my-3">+ Tambah</a>
<table class="table" id="table">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Name</th>
      <th scope="col">Price</th>
      <th scope="col">Description</th>
      <th scope="col">Kategori</th>
      <th scope="col">Action</th>
    </tr>
  </thead>
  <tbody>
      @forelse ($product as $key=>$value)
        <tr>
            <td>{{$key + 1}}</th>
            <td>{{$value->name}}</td>
            <td>{{$value->price}}</td>
            <td>{{$value->description}}</td>
            <td>{{$value->categories_id}}</td>
            <td>

                <form action="/product/{{$value->id}}" method="POST">
                    @csrf
                    @method('DELETE')
                    <a href="/product/{{$value->id}}" class="btn btn-info">Detail</a>
                    <a href="/product/{{$value->id}}/edit" class="btn btn-primary">Edit</a>
                    <input type="submit" class="btn btn-danger my-1" value="Delete">
                </form>
            </td>
        </tr>
    @empty
        <tr colspan="3">
            <td>No data</td>
        </tr>
    @endforelse
  </tbody>
</table>
@endsection

@push('script')
<script src="//cdn.datatables.net/1.13.5/js/jquery.dataTables.min.js"></script>
<script>
    $(document).ready(function (){
        $('#table').DataTable();
    });
</script>
@endpush
