<?php

use App\Http\Controllers\CategoryController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\ProductGaleriesController;
use App\Http\Controllers\TransactionController;
use App\Http\Controllers\TransDetailController;
use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use RealRashid\SweetAlert\Facades\Alert;



/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function (Request $request) {
    $user = $request->user();
    $categories = DB::table('categories')->get();
    $product = DB::table('products')->get();
    $productGaleries = DB::table('product_galeries')->get();
    return view('home', ['user' => $user, 'categories' => $categories, 'product' => $product, 'productGaleries' => $productGaleries]);
});

Route::get('/admin', function () {
    return view('masters.admin');
});

Route::get('/profile', function (Request $request) {
    $user = $request->user();
    return view('profile.profile', ['user' => $user]);
});

Route::get('/profile/edit', function (Request $request) {
    $user = $request->user();

    return view('profile.edit', ['user' => $user]);
});

Route::put('/profile', function (Request $request) {
    $user = $request->user();
    $request->validate([
        'name' => 'required',
        'no_phone' => 'required',
        'address' => 'required',
    ]);

    DB::table('users')->where('id', $user->id)->update(
                [
                    'name' => $request['name'],
                    'no_phone' => $request['no_phone'],
                    'address' => $request['address'],
                ]);

    Alert::success('Success', 'Sukses Mengedit Profil');
    return redirect('profile');

});

//CRUD Produk
Route::resource('product', ProductController::class);

//CRUD Kategori
Route::get('/categories', [CategoryController::class, 'index']);
Route::get('/categories/create', [CategoryController::class, 'create']);
Route::post('/categories', [CategoryController::class, 'store']);
Route::delete('/categories/{categories_id}', [CategoryController::class, 'destroy']);
Route::get('/categories/{categories_id}/edit', [CategoryController::class, 'edit']);
Route::put('/categories/{categories_id}', [CategoryController::class, 'update']);
//end of CRUD Kategori

Auth::routes();

// Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::resource('product-galeries', ProductGaleriesController::class);

// transaction
Route::resource('transaction', TransactionController::class);
Route::resource('transaction-details', TransDetailController::class);
